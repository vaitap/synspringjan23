Lab6 - parameters processing using RequestParam
	Create params.RequestParamDemo.java
		package params;

		import org.springframework.stereotype.Controller;
		import org.springframework.web.bind.annotation.GetMapping;
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.PostMapping;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestParam;
		import org.springframework.web.bind.annotation.ResponseBody;

		@Controller
		@RequestMapping(value = "/rp")
		public class RequestParamDemo {
			
			@PostMapping(value="/hello")
			@ResponseBody
			public String sayhello(@RequestParam(name = "nm")String name){
				System.out.println("in RequestParamDemo - Hello with name " + name);
				return "<h1>Hello, " + name  + "</h1>";
			}
			@GetMapping(value="/add")
			@ResponseBody
			public String add(@RequestParam(name = "n1")int no1,
					@RequestParam(name="n2")int no2
					){
				System.out.println("in RequestParamDemo - Add with " + no1  + ", " + no2);
				return "<h1>Sum =  " + (no1+no2) + "</h1>";
			}
		}
	Modify index.html to include
	
		<form action="/Day2.demo1.web/app/rp/hello" method="POST">
		UserName : <input type="text" name="nm"/><br/>
		<input type="submit" value="Invoke RequestParamDemo"> 
		</form>
		<hr>
		<form action="/Day2.demo1.web/app/rp/add" method="GET">
		Number1 : <input type="number" name="n1"/><br/>
		Number1 : <input type="number" name="n2"/><br/>
		<input type="submit" value="Invoke RequestParamDemo Add"> 
		</form>

	Test both the options with postman
Lab5 - parameters processing using PathVariable
	Create a new class params.PathParmDemo.java
		package params;

		import org.springframework.stereotype.Controller;
		import org.springframework.web.bind.annotation.GetMapping;
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.ResponseBody;

		@Controller
		@RequestMapping(value = "/pp")
		public class PathParmDemo {
			
			@GetMapping(value="/hello/{nm}")
			@ResponseBody
			public String sayhello(@PathVariable(name = "nm")String name){
				System.out.println("in PathParamDemo - Hello with name " + name);
				return "<h1>Hello, " + name  + "</h1>";
			}
			@GetMapping(value="/add/{n1}/{n2}")
			@ResponseBody
			public String add(@PathVariable(name = "n1")int no1,
										@PathVariable(name="n2")int no2
					){
				System.out.println("in PathParamDemo - Add with " + no1  + ", " + no2);
				return "<h1>Sum =  " + (no1+no2) + "</h1>";
			}
		}

	Modify index.html to include 
		<hr>
		<h1><a href="http://localhost:8080/Day2.demo1.web/app/pp/hello/Vaishali">Invoke PathVariable hello demo with Vaishali</a>
		</h1>
		<hr>
		<h1><a href="http://localhost:8080/Day2.demo1.web/app/pp/add/333/3333">Invoke PathVariable add demo with 333 and 3333</a>
		</h1>
	Modify xx-servlet.xml to include 
		<context:component-scan base-package="demo, params"></context:component-scan>
	Test by clicking hlinks on index.html
	
Lab4 - https://www.w3schools.com/html/html_forms.asp
	create a form to accept name and age

Lab3 - in same project 
	modify demo-servlet.xml to include
			<context:component-scan base-package="demo"></context:component-scan>
	Write demo.SecondController.java
		package demo;
		import javax.servlet.http.HttpServletRequest;
		import javax.servlet.http.HttpServletResponse;

		import org.springframework.stereotype.Controller;
		import org.springframework.web.bind.annotation.GetMapping;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.servlet.ModelAndView;

		@Controller
		@RequestMapping(value = "/second")
		public class SecondController {
			@GetMapping
			public String method1(HttpServletRequest request, HttpServletResponse response) throws Exception {
				System.out.println("in method1 of SecondController");
				response.getWriter().print("<h1>From Second Controller</h1>");
				return "/t1.jsp";
			}
			@GetMapping(value="/s1")
			public String method2(HttpServletRequest request, HttpServletResponse response) throws Exception {
				System.out.println("in method2 of SecondController");
				response.getWriter().print("<h1>From Second Controller</h1>");
				return "/t1.jsp";
			}
			
			@GetMapping(value="/s2")
			public String method3(HttpServletRequest request, HttpServletResponse response) throws Exception {
				System.out.println("in method3 of SecondController");
				response.getWriter().print("<h1>From Second Controller</h1>");
				return "/t1.jsp";
			}
		}
	URL -> http://localhost:8080/Day2.demo1.web/app/second
			http://localhost:8080/Day2.demo1.web/app/second/s1
			http://localhost:8080/Day2.demo1.web/app/second/s2
			
Lab2 - Create a new maven project -> simple project
			Day2, Day2.demo1.web
			Packaging - war 
	    Modify pom.xml to include
			<dependencies>
				<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
				<dependency>
					<groupId>org.springframework</groupId>
					<artifactId>spring-webmvc</artifactId>
					<version>5.2.22.RELEASE</version>
				</dependency>
			</dependencies>
		Create web.xml in src/main/webapp/WEB-INF
			<?xml version="1.0" encoding="UTF-8"?>
			<web-app version="4.0" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
			   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			   xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee 
			   http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd">

				<servlet>
					<servlet-name>demo</servlet-name>
					<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
				   <load-on-startup>1</load-on-startup>
				</servlet>

				<servlet-mapping>
					<servlet-name>demo</servlet-name>
					<url-pattern>/app/*</url-pattern>
				</servlet-mapping>
			</web-app>
		create demo-servlet.xml in src/main/webapp/WEB-INF
			<?xml version="1.0" encoding="UTF-8"?>
			<beans xmlns="http://www.springframework.org/schema/beans"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xmlns:context="http://www.springframework.org/schema/context"
				xsi:schemaLocation="http://www.springframework.org/schema/beans
					https://www.springframework.org/schema/beans/spring-beans.xsd
					http://www.springframework.org/schema/context
					https://www.springframework.org/schema/context/spring-context.xsd">
				
					<bean name="/first" class="FirstController"></bean>
			</beans>

		create demo.FirstController.java
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;

			import org.springframework.web.servlet.ModelAndView;
			import org.springframework.web.servlet.mvc.Controller;

			public class FirstController implements Controller {

				public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
					System.out.println("in HandlerRequest of FirstController");
					response.getWriter().print("<h1>From Controller</h1>");
					return new ModelAndView("/t1.jsp");
				}
			}
		Create a new jsp (t1.jsp in webapp)
			<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
				pageEncoding="ISO-8859-1"%>
			<!DOCTYPE html>
			<html>
			<head>
			<meta charset="ISO-8859-1">
			<title>Insert title here</title>
			</head>
			<body>
			<h1>T1 JSP</h1>
			</body>
			</html>
		
		Run on server -> http://localhost:8080/Day2.demo1.web/app/first
------------		
Lab1 - Download Tomcat (https://tomcat.apache.org/download-90.cgi)
		extract it on c:\work\apache-tomcat
	
		Ecplipse -> new project (other)=> Dynamic Web Project
			Next-> Target Runtime -> 
				new runtime-> 	apache -> tomcat 9
						select checkbox for create new local server
						jre - 1.8(default)
			in project -> new html -> index.html
					add a line in body tags- <h1>Hello World !!!</h1>
			in project -> new servlet -> First
				modify do get method
				response.getWriter().append("<h1>Served at: ").append(request.getContextPath()).append("</h1>");
			Rightclick on index.html -> run on server
			Rightclick on servlet -> run on server
			